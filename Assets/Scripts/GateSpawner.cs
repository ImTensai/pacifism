﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateSpawner : MonoBehaviour
{
    public GameObject gatePrefab;
    public int maxGates;
    public float interval;

    private GameObject[] gates;

    void Start()
    {
        gates = new GameObject[maxGates];

        for (int i = 0; i < maxGates; i++)
        {
            GameObject g = Instantiate(gatePrefab);
            g.SetActive(false);
            gates[i] = g;
        }

        StartCoroutine(SpawnCoroutine());
    }


    public GameObject Spawn()
    {
        for (int t = 0; t < maxGates; t++)
        {
            if (!gates[t].activeSelf)
            {
                return gates[t];
            }
        }
        return null;
    }

    IEnumerator SpawnCoroutine()
    {
        while (enabled)
        {
            Vector3 pos = new Vector3(Random.Range(-10f, 10f), 0, Random.Range(-10f, 10f));
            Vector3 rot = new Vector3(0, Random.Range(0f, 360f),0);
            ParticleManager.particleManager.Play("gateSpawnParticles", pos);
            yield return new WaitForSeconds(3f);

            GameObject g = Spawn();

            if (g != null)
            {
                g.transform.position = pos;
                g.SetActive(true);
                g.transform.rotation = Quaternion.Euler(rot);
                //AudioManager.Play("spawnGate");
            }
            yield return new WaitForSeconds(interval);
        }
    }
}
