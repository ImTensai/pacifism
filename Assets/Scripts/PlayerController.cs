﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController player;
    public float speed = 7;

    private Rigidbody body;

    public static Vector3 position;

    void Awake()
    {
        if (player == null)
        {
            player = this;
        }
        
    }

    private void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    void Update()   //Player Movement
    {
        position = player.transform.position;
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        Vector3 dir = new Vector3(x, 0, z);

        float d = dir.sqrMagnitude;
        if (d > 1) dir.Normalize();
        if (d > 0.1f)
        {
            transform.forward = dir;
        }
        body.velocity = dir * speed;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<EnemyController>())
        {
            Die();
        }
    }

    public static void Die ()
    {
        ParticleManager.particleManager.Play("deathParticles", player.transform.position);
        AudioManager.Play("Explosion");
        ScoreScript.GameOver();
        player.gameObject.SetActive(false);
    }
}
