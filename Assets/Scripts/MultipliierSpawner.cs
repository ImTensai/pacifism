﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipliierSpawner : MonoBehaviour
{
    public static MultipliierSpawner instance;
    public GameObject multiplierPrefab;
    public int maxMulitiplier = 100;
    public float interval = 3;

    private GameObject[] multipliers;

    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
        multipliers = new GameObject[maxMulitiplier];

        for (int i = 0; i < maxMulitiplier; i++)
        {
            GameObject g = Instantiate(multiplierPrefab);
            g.SetActive(false);
            multipliers[i] = g;
        }
    }


    public GameObject Spawn()
    {
        for (int t = 0; t < maxMulitiplier; t++)
        {
            if (!multipliers[t].activeSelf)
            {
                multipliers[t].SetActive(true);
                return multipliers[t];
            }
        }
        return null;
    }
}
