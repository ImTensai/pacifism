﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;
    public int maxEnemies;
    public float interval;

    private GameObject[] enemies;

    void Start()
    {
        enemies = new GameObject[maxEnemies];

        for (int i = 0; i < maxEnemies; i++)
        {
            GameObject g = Instantiate(enemyPrefab);
            g.SetActive(false);
            enemies[i] = g;
        }
        StartCoroutine(SpawnCoroutine());
    }


    public GameObject Spawn()
    {
        for (int t = 0; t < maxEnemies; t ++)
        {
            if (!enemies[t].activeSelf)
            {
                return enemies[t];
            }
        }
        return null;
    }

    IEnumerator SpawnCoroutine()
    {
        while (enabled)
        {
            Vector3 pos = Vector3.zero;

            yield return new WaitForSeconds(interval);

            for(int i = 0; i < 2; i++)
            {
                pos = new Vector3(Random.Range(-10f, 10f), 0, Random.Range(-10f, 10f));
                ParticleManager.particleManager.Play("enemySpawnParticles", pos);
                yield return new WaitForSeconds(1);

                for (int t = 0; t < 3; t++)
                {
                    GameObject g = Spawn();

                    if (g != null)
                    {
                        g.transform.position = pos;
                        g.SetActive(true);
                        //AudioManager.Play("spawnGate");
                    }
                }
            }
        }
        yield return new WaitForEndOfFrame();
    }
}
