﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    public static ParticleManager particleManager;

    public ParticleSystem[] particlePrefabs;
    public int maxParticles = 20;

    public ParticleSystem[][] systems;

    private int[] indices;

    private void Awake()
    {
        if (particleManager == null)
        {
            particleManager = this;
        }
        else
        {
            Destroy(this);
        }

        int count = particlePrefabs.Length;
        systems = new ParticleSystem[count][];
        indices = new int[count];

        {
            for (int i = 0; i < count; i++)
            {
                systems[i] = new ParticleSystem[maxParticles];
                
                for (int j = 0; j < maxParticles; j++)
                {
                    systems[i][j] = Instantiate(particlePrefabs[i]);
                }
            }
        }
    }

    public ParticleSystem Play(string systemName, Vector3 position)
    {
     
        for (int t = 0; t < particleManager.particlePrefabs.Length; t++)
        {
            if(particleManager.particlePrefabs[t].name == systemName)
            {
                ParticleSystem[] pool = systems[t];
                int index = indices[t];
                ParticleSystem system = pool[index];
                system.transform.position = position;
                system.Play();
                indices[t] = (index + 1) % particleManager.maxParticles;
                return system;
            }
        }
        return null;
    }
}
