﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MenuController : MonoBehaviour
{
    private GameObject lastSelection;

    private void Awake()
    {
        Cursor.visible = false;
    }

    public void SeclectionChanged()
    {
        GameObject g = EventSystem.current.currentSelectedGameObject;
        lastSelection = g;
    }

    public void StartPacifism()
    {
        SceneManager.LoadScene("Pacifism");
    }

    /*
    public void StartWarmonger()
    {
        SceneManager.LoadScene("Warmonger");
    }
    */

    public void ExitGame()
    {
        Debug.Log("Game Stopped");
        Application.Quit();
    }
}
