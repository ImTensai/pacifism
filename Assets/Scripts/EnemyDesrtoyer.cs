﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDesrtoyer : MonoBehaviour
{
    private void OnTriggerEnter(Collider enemy)
    {
        if (enemy.GetComponent<EnemyController>())
        {
            enemy.gameObject.SetActive(false);
            AudioManager.Play("Explosion");
            ScoreScript.ScorePoints();
            ScoreScript.SpawnMultiplier(enemy.transform.position);
        }
    }
}
