﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour
{
    public GameObject explosionCollider;

    public float waitTime;

    IEnumerator ExplosionTime()
    {
        explosionCollider.SetActive(true);
        yield return new WaitForSeconds(waitTime);
        explosionCollider.SetActive(false);
    }

    public void Explosion()
    {
        StartCoroutine(ExplosionTime());
    }
}
