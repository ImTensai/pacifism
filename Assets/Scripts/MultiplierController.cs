﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplierController : MonoBehaviour
{
    private GameObject player;

    public float lifetime = 5;
    public float pullRange = 20f;
    public float speed = 5f;
    private void Start()
    {
        if (player == null)
        {
            player = FindObjectOfType<PlayerController>().gameObject;
        }
        StartCoroutine(MultiplierLifetime());
    }
    private void Update()
    {
        if (player == null) return;
        if(Vector3.Distance(player.transform.position,transform.position) < pullRange && player != null && player.activeSelf)
        {
            Vector3 direction = player.transform.position - transform.position;
            transform.Translate(direction.normalized * speed * Time.deltaTime);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>())
        {
            ScoreScript.AddtoMultiplier();
            gameObject.SetActive(false);
            AudioManager.Play("Multiplier Pickup");
        }
    }

    IEnumerator MultiplierLifetime()
    {
        yield return new WaitForSeconds(5);
        gameObject.SetActive(false);
        yield return new WaitForEndOfFrame();
    }
}
