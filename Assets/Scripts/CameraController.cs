﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float maxCameraTiltX = 20;
    public float maxCameraTiltZ = 20;
    public float playerRange = 25;

    private void Update()
    {
        Vector3 playerPos = PlayerController.player.transform.position;
        float newCamAngleX = 0;
        float newCamAngleZ = 0;

        if (playerPos.x < 0)
        {
            newCamAngleX = Mathf.Lerp(0, -maxCameraTiltX, (Mathf.Abs(playerPos.x)) / playerRange);
        }
        else if (playerPos.x > 0)
        {
            newCamAngleX = Mathf.Lerp(0, maxCameraTiltX, (Mathf.Abs(playerPos.x)) / playerRange);
        }

        if (playerPos.z < 0)
        {
            newCamAngleZ = Mathf.Lerp(0, -maxCameraTiltZ, (Mathf.Abs(playerPos.z)) / playerRange);
        }

        else if (playerPos.z > 0)
        {
            newCamAngleZ = Mathf.Lerp(0, maxCameraTiltZ, (Mathf.Abs(playerPos.z)) / playerRange);
        }

        Vector3 rootAngle = new Vector3(newCamAngleZ, 0, -newCamAngleX);

        transform.position = new Vector3(playerPos.x, transform.position.y, playerPos.z);
        transform.eulerAngles = rootAngle;
    }
}
