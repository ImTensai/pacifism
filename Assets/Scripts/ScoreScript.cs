﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreScript : MonoBehaviour
{
    public static ScoreScript instance;

    public Image gameOverImage;
    public Text gameOverText;
    public string[] gameOverTexts = new string[4];

    public Text scoreNumber;
    public Text highScoreNumber;
    public Text multiplierText;

    public int chanceToDropMultiplier = 20;
    public static int points = 150;
    public static int score = 0;
    public static int highScore = 0;
    public static int multiplier = 0;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        AudioManager.PlayMainMusic();
        gameOverImage.enabled = false;
        gameOverText.enabled = false;
        //Set Highscore if there already is one (PlayerPref.)
        multiplier = 0;
        score = 0;
        scoreNumber.text = score.ToString();
        highScoreNumber.text = highScore.ToString();
        multiplierText.text = "X " + multiplier;
    }

    public static void ScorePoints()
    {
        
        score += points * multiplier;
        instance.scoreNumber.text = score.ToString();

        if ( score > highScore)
        {
            highScore = score;
            //Save highscore
            instance.highScoreNumber.text = highScore.ToString();
        }
    }

    public static void AddtoMultiplier()
    {
        multiplier++;
        instance.multiplierText.text = "x" + multiplier;
    }

    public static void SpawnMultiplier(Vector3 position)
    {
        if (Random.Range(0, 100) <= instance.chanceToDropMultiplier)
        {
            GameObject go = MultipliierSpawner.instance.Spawn();
            go.transform.position = position;
        }
    }

    public static void GameOver ()
    {
        instance.StartCoroutine(instance.GameOverCoroutine());
    }

    IEnumerator GameOverCoroutine ()
    {
        gameOverImage.enabled = true;
        gameOverText.enabled = true;
        gameOverText.text = gameOverTexts[Random.Range(0, gameOverTexts.Length)];
        yield return new WaitForSeconds(4);
        score = 0;
        multiplier = 0;
        multiplierText.text = "X " + multiplier;
        SceneManager.LoadScene("PacifismStartMenu");
    }
}