﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GateController : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<PlayerController>())
        {
            PlayerController.Die();
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.GetComponent<PlayerController>())
        {
            ParticleManager.particleManager.Play("explosionParticles", transform.position).gameObject.GetComponent<ExplosionController>().Explosion();
            if (transform.parent)
            {
                transform.parent.gameObject.SetActive(false);
            }
        }
    }
}
