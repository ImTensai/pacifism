﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private Rigidbody body;

    public float enemySpeed = 6f;

    void Start()
    {
        body = GetComponent<Rigidbody>();
    }
       

    void Update()
    {
        Vector3 dir = PlayerController.position - transform.position;

        if (dir.sqrMagnitude > 1)
        {
            dir.Normalize();
        }

        transform.forward = dir;
        body.velocity = dir * enemySpeed;
    }
}
